<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $table = 'transaction';
    public $timestamps = false;
    protected $attributes = [
        'transactionId' => 0,
        'from_userId' => 0,
        'to_userId' => 0,
        'from_walletId' => 0,
        'to_walletId' => 0,
        'from_account' => 0,
        'to_account' => 0,
        'transaction_type' => '',
        'transaction_amount' => 0,
        'transaction_time' => '',
        'description' => null,
    ];
}
