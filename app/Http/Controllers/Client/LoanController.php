<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Loan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class LoanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        return view('client/loan/index');
    }

    public function get(Request $request)
    {
        $data = Loan::get();
        return response()->json($data);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required|integer',
            'loan_status' => 'required|integer',
            'loan_amount' => 'required|numeric',
            'loan_type' => 'required|string|max:50',
            'agent_name' => 'required|string|max:50',
            'rate_of_Interest' => 'required|numeric',
            'duration' => 'required|string|max:50',
            'l_update' => 'required|integer',
            'image' => 'nullable|string|max:5000',
            'reqinr' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return Response::json([
                'status' => false,
                'message' => $validator->errors(),
            ]);
        }

        $loan = new Loan();
        $loan->userId = $request->input('userId');
        $loan->loan_status = $request->input('loan_status');
        $loan->loan_amount = $request->input('loan_amount');
        $loan->loan_type = $request->input('loan_type');
        $loan->agent_name = $request->input('agent_name');
        $loan->rate_of_Interest = $request->input('rate_of_Interest');
        $loan->duration = $request->input('duration');
        $loan->l_update = $request->input('l_update');
        $loan->image = $request->input('image');
        $loan->reqinr = $request->input('reqinr');
        $loan->save();
        $loan->loanId = $loan->id;

        return response()->json($loan);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'loanId' => 'required|integer|exists:loan,loanId',
            'userId' => 'required|integer',
            'loan_status' => 'required|integer',
            'loan_amount' => 'required|numeric',
            'loan_type' => 'required|string|max:50',
            'agent_name' => 'required|string|max:50',
            'rate_of_Interest' => 'required|numeric',
            'duration' => 'required|string|max:50',
            'l_update' => 'required|integer',
            'image' => 'nullable|string|max:5000',
            'reqinr' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return Response::json([
                'status' => false,
                'message' => $validator->errors(),
            ], 400);
        }

        Loan::where('loanId', $request->input('loanId'))->update([
            'userId' => $request->input('userId'),
            'loan_status' => $request->input('loan_status'),
            'loan_amount' => $request->input('loan_amount'),
            'loan_type' => $request->input('loan_type'),
            'agent_name' => $request->input('agent_name'),
            'rate_of_Interest' => $request->input('rate_of_Interest'),
            'duration' => $request->input('duration'),
            'l_update' => $request->input('l_update'),
            'image' => $request->input('image'),
            'reqinr' => $request->input('reqinr'),
        ]);

        return response()->json(Loan::where('loanId', $request->input('loanId'))->first());
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'loanId' => 'required|integer|exists:loan,loanId',
        ]);

        if ($validator->fails()) {
            return Response::json([
                'status' => false,
                'message' => $validator->errors(),
            ], 400);
        }

        Loan::where('loanId', $request->input('loanId'))->delete();

        return response()->json([
            'status' => true,
            'message' => 'Data deleted successfully',
        ]);
    }
}
