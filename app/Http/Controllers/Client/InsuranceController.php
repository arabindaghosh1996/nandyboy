<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Insurance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class InsuranceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        return view('client/insurance/index');
    }

    public function get(Request $request)
    {
        $data = Insurance::get();
        return response()->json($data);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required|integer',
            'insurance_status' => 'required|integer',
            'insurance_amount' => 'required|numeric',
            'insurance_type' => 'required|string|max:50',
            'agent_name' => 'required|string|max:100',
            'duration' => 'required|string|max:50',
            'i_update' => 'required|integer',
            'image' => 'nullable|string|max:5000',
            'wallet' => 'required|string',
            'currency' => 'required|string',
        ]);

        if ($validator->fails()) {
            return Response::json([
                'status' => false,
                'message' => $validator->errors(),
            ]);
        }

        $insurance = new Insurance();
        $insurance->userId = $request->input('userId');
        $insurance->insurance_status = $request->input('insurance_status');
        $insurance->insurance_amount = $request->input('insurance_amount');
        $insurance->insurance_type = $request->input('insurance_type');
        $insurance->agent_name = $request->input('agent_name');
        $insurance->duration = $request->input('duration');
        $insurance->i_update = $request->input('i_update');
        $insurance->image = $request->input('image');
        $insurance->wallet = $request->input('wallet');
        $insurance->currency = $request->input('currency');
        $insurance->save();
        $insurance->insuranceId = $insurance->id;

        return response()->json($insurance);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'insuranceId' => 'required|integer|exists:insurance,insuranceId',
            'userId' => 'required|integer',
            'insurance_status' => 'required|integer',
            'insurance_amount' => 'required|numeric',
            'insurance_type' => 'required|string|max:50',
            'agent_name' => 'required|string|max:100',
            'duration' => 'required|string|max:50',
            'i_update' => 'required|integer',
            'image' => 'nullable|string|max:5000',
            'wallet' => 'required|string',
            'currency' => 'required|string',
        ]);

        if ($validator->fails()) {
            return Response::json([
                'status' => false,
                'message' => $validator->errors(),
            ], 400);
        }

        Insurance::where('insuranceId', $request->input('insuranceId'))->update([
            'userId' => $request->input('userId'),
            'insurance_status' => $request->input('insurance_status'),
            'insurance_amount' => $request->input('insurance_amount'),
            'insurance_type' => $request->input('insurance_type'),
            'agent_name' => $request->input('agent_name'),
            'duration' => $request->input('duration'),
            'i_update' => $request->input('i_update'),
            'image' => $request->input('image'),
            'wallet' => $request->input('wallet'),
            'currency' => $request->input('currency'),
        ]);

        return response()->json(Insurance::where('insuranceId', $request->input('insuranceId'))->first());
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'insuranceId' => 'required|integer|exists:insurance,insuranceId',
        ]);

        if ($validator->fails()) {
            return Response::json([
                'status' => false,
                'message' => $validator->errors(),
            ], 400);
        }

        Insurance::where('insuranceId', $request->input('insuranceId'))->delete();

        return response()->json([
            'status' => true,
            'message' => 'Data deleted successfully',
        ]);
    }
}
