<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class TransactionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        return view('client/transaction/index');
    }

    public function get(Request $request)
    {
        $data = Transaction::get();
        return response()->json($data);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'transactionId' => 'required|integer|exists:transaction,transactionId',
            'from_userId' => 'required|integer',
            'to_userId' => 'required|integer',
            'from_walletId' => 'required|integer',
            'to_walletId' => 'required|integer',
            'from_account' => 'required|numeric',
            'to_account' => 'required|numeric',
            'transaction_type' => 'required|string|max:100',
            'transaction_amount' => 'required|numeric',
            'transaction_time' => 'required|date_format:Y-m-d H:i:s',
            'description' => 'nullable|string|max:255',
        ]);

        if ($validator->fails()) {
            return Response::json([
                'status' => false,
                'message' => $validator->errors(),
            ], 400);
        }

        Transaction::where('transactionId', $request->input('transactionId'))->update([
            'from_userId' => $request->input('from_userId'),
            'to_userId' => $request->input('to_userId'),
            'from_walletId' => $request->input('from_walletId'),
            'to_walletId' => $request->input('to_walletId'),
            'from_account' => $request->input('from_account'),
            'to_account' => $request->input('to_account'),
            'transaction_type' => $request->input('transaction_type'),
            'transaction_amount' => $request->input('transaction_amount'),
            'transaction_time' => $request->input('transaction_time'),
            'description' => $request->input('description'),
        ]);

        return response()->json(Transaction::where('transactionId', $request->input('transactionId'))->first());
    }

    // public function delete(Request $request)
    // {
    //     $validator = Validator::make($request->all(), [
    //         'transactionId' => 'required|integer|exists:transaction,transactionId',
    //     ]);

    //     if ($validator->fails()) {
    //         return Response::json([
    //             'status' => false,
    //             'message' => $validator->errors(),
    //         ], 400);
    //     }

    //     Transaction::where('transactionId', $request->input('transactionId'))->delete();

    //     return response()->json([
    //         'status' => true,
    //         'message' => 'Data deleted successfully',
    //     ]);
    // }
}
