<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class WalletController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        return view('client/wallet/index');
    }

    public function get(Request $request)
    {
        $data = Wallet::get();
        return response()->json($data);
    }

    public function get_all_name(Request $request)
    {
        $data = Wallet::get();
        return response()->json($data);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'walletId' => 'required|integer|exists:wallet,walletId',
            'coin' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return Response::json([
                'status' => false,
                'message' => $validator->errors(),
            ], 400);
        }

        Wallet::where('walletId', $request->input('walletId'))->update([
            'coin' => $request->input('coin'),
        ]);

        return response()->json(Wallet::where('walletId', $request->input('walletId'))->first());
    }
}
