<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Bond;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class BondController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        return view('client/bond/index');
    }

    public function get(Request $request)
    {
        $data = Bond::get();
        return response()->json($data);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required|integer',
            'bond_name' => 'required|string|max:50',
            'bond_amount' => 'required|integer',
            'bond_type' => 'required|string|max:50',
            'history' => 'required|string|max:10000',
        ]);

        if ($validator->fails()) {
            return Response::json([
                'status' => false,
                'message' => $validator->errors(),
            ]);
        }

        $bond = new Bond();
        $bond->userId = $request->input('userId');
        $bond->bond_name = $request->input('bond_name');
        $bond->bond_amount = $request->input('bond_amount');
        $bond->bond_type = $request->input('bond_type');
        $bond->history = $request->input('history');
        $bond->save();
        $bond->bondId = $bond->id;

        return response()->json($bond);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'bondId' => 'required|integer|exists:bond,bondId',
            'userId' => 'required|integer',
            'bond_name' => 'required|string|max:50',
            'bond_amount' => 'required|integer',
            'bond_type' => 'required|string|max:50',
            'history' => 'required|string|max:10000',
        ]);

        if ($validator->fails()) {
            return Response::json([
                'status' => false,
                'message' => $validator->errors(),
            ], 400);
        }

        Bond::where('bondId', $request->input('bondId'))->update([
            'userId' => $request->input('userId'),
            'bond_name' => $request->input('bond_name'),
            'bond_amount' => $request->input('bond_amount'),
            'bond_type' => $request->input('bond_type'),
            'history' => $request->input('history'),
        ]);

        return response()->json(Bond::where('bondId', $request->input('bondId'))->first());
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'bondId' => 'required|integer|exists:bond,bondId',
        ]);

        if ($validator->fails()) {
            return Response::json([
                'status' => false,
                'message' => $validator->errors(),
            ], 400);
        }

        Bond::where('bondId', $request->input('bondId'))->delete();

        return response()->json([
            'status' => true,
            'message' => 'Data deleted successfully',
        ]);
    }
}
