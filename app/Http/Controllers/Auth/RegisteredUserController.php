<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:user'],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
            'agent' => ['required', 'string', 'in:yes,no'],
            'phone_num' => ['required', 'string', 'max:20'],
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => md5($request->password),
            'agent' => $request->agent,
            'lastLogin' => date('Y-m-d H:i:s'),
            'phone_num' => $request->phone_num,
            'occupation' => '',
            'adhar_number' => '',
            'annual_Income' => '',
            'dob' => '',
            'address' => '',
            'wallet1' => 0,
            'wallet2' => 0,
            'wallet3' => 0,
            'wallet4' => 0,
            'wallet5' => 0,
            'referal_code' => '',
            'refered_by' => 0,
            'admin' => '',
            'cheque' => 0,
            'token' => '',
            'aadharf' => '',
            'aadharb' => '',
            'licensef' => '',
            'licenseb' => '',
            'pan' => '',
        ]);

        event(new Registered($user));

        Auth::login($user);

        return redirect(RouteServiceProvider::HOME);
    }
}
