<?php

use App\Http\Controllers\Client\MainController;
use App\Http\Controllers\Client\TransactionController;
use App\Http\Controllers\Client\UserController;
use App\Http\Controllers\Client\WalletController;
use App\Http\Controllers\Client\BondController;
use App\Http\Controllers\Client\InsuranceController;
use App\Http\Controllers\Client\LoanController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');

require __DIR__ . '/auth.php';



// Cache routes
Route::get('clear-cache', function () {
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('view:clear');
    $exitCode = Artisan::call('config:cache');
    return true;
});


Route::group(['middleware' => 'auth'], function () {
    Route::get('dashboard', function () {
        // return view('dashboard');
        return redirect()->route('client/bond');
    })->name('client/dashboard');

    Route::get('/', function () {
        return redirect()->route('client/bond');
        // return view('dashboard');
    })->name('client/dashboard');

    Route::get('transaction', [TransactionController::class, 'index'])->name('client/transaction');
    Route::get('transaction/get', [TransactionController::class, 'get'])->name('client/transaction/get');
    Route::post('transaction/update', [TransactionController::class, 'update'])->name('client/transaction/update');
    // Route::post('transaction/delete', [TransactionController::class, 'delete'])->name('client/transaction/delete');


    Route::get('user', [UserController::class, 'index'])->name('client/user');
    Route::get('user/get', [UserController::class, 'get'])->name('client/user/get');
    Route::get('user/get_all_name', [UserController::class, 'get_all_name'])->name('client/user/get_all_name');

    Route::get('wallet', [WalletController::class, 'index'])->name('client/wallet');
    Route::get('wallet/get', [WalletController::class, 'get'])->name('client/wallet/get');
    Route::get('wallet/get_all_name', [WalletController::class, 'get_all_name'])->name('client/wallet/get_all_name');
    Route::post('wallet/update', [WalletController::class, 'update'])->name('client/wallet/update');

    Route::get('main/get_all_coin_name', [MainController::class, 'get_all_coin_name'])->name('client/main/get_all_coin_name');

    Route::group(['prefix' => 'bond'], function () {
        Route::get('/', [BondController::class, 'index'])->name('client/bond');
        Route::get('/get', [BondController::class, 'get'])->name('client/bond/get');
        Route::post('/create', [BondController::class, 'create'])->name('client/bond/create');
        Route::post('/update', [BondController::class, 'update'])->name('client/bond/update');
        Route::post('/delete', [BondController::class, 'delete'])->name('client/bond/delete');
    });

    Route::group(['prefix' => 'insurance'], function () {
        Route::get('/', [InsuranceController::class, 'index'])->name('client/insurance');
        Route::get('/get', [InsuranceController::class, 'get'])->name('client/insurance/get');
        Route::post('/create', [InsuranceController::class, 'create'])->name('client/insurance/create');
        Route::post('/update', [InsuranceController::class, 'update'])->name('client/insurance/update');
        Route::post('/delete', [InsuranceController::class, 'delete'])->name('client/insurance/delete');
    });

    Route::group(['prefix' => 'loan'], function () {
        Route::get('/', [LoanController::class, 'index'])->name('client/loan');
        Route::get('/get', [LoanController::class, 'get'])->name('client/loan/get');
        Route::post('/create', [LoanController::class, 'create'])->name('client/loan/create');
        Route::post('/update', [LoanController::class, 'update'])->name('client/loan/update');
        Route::post('/delete', [LoanController::class, 'delete'])->name('client/loan/delete');
    });
});
