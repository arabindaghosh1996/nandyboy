@extends('layouts.main')

@section('content')
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-news-paper icon-gradient bg-asteroid">
                    </i>
                </div>
                <div>Bonds
                    <div class="page-title-subheading">
                        View all bonds
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tabs-animation">
        <div class="mb-3 card">
            <div class="card-body">
                <table id="main_table" class="table table-bordered table-hover dt-responsive nowrap"></table>
            </div>
        </div>
    </div>


    <script>
        window.lib = {
            table: null,
            load_count: 0,
            columnDefs: {},
            user_options: {},
        };

        $(document).ready(function() {

            // Load data from the server
            $.ajax({
                url: "{{ route('client/user/get_all_name') }}",
                type: 'GET',
                dataType: 'json',
                success: function(response) {
                    response.forEach(function(item) {
                        lib.user_options[item.userId] = item.name;
                    });

                    lib.load_count++;
                    load_done();
                },
                processData: false,
                contentType: false,
            });
        });

        function load_done() {
            if (lib.load_count >= 1) {
                load_config();
                load_table();
            }
        }

        function load_config() {
            lib.columnDefs = [{
                    data: 'bondId',
                    title: 'bondId',
                    type: 'readonly',
                },
                {
                    data: 'userId',
                    title: 'userId',
                    type: 'select',
                    options: lib.user_options,
                    select2: {
                        width: '100%'
                    },
                    render: function(data, type, row, meta) {
                        if (data == null || !(data in lib.user_options)) return null;
                        return lib.user_options[data];
                    },
                },
                {
                    data: 'bond_name',
                    title: 'bond_name',
                },
                {
                    data: 'bond_amount',
                    title: 'bond_amount',
                },
                {
                    data: 'bond_type',
                    title: 'bond_type',
                },
                {
                    data: 'history',
                    title: 'history',
                },
            ];
        }

        function load_table() {
            lib.table = $('#main_table').DataTable({
                'sPaginationType': 'full_numbers',
                ajax: {
                    url: "{{ route('client/bond/get') }}",
                    dataSrc: ''
                },
                columns: lib.columnDefs,
                dom: 'Bfrtip',
                select: 'single',
                responsive: true,
                altEditor: true,
                buttons: [{
                        text: 'Add',
                        name: 'add',
                    },
                    {
                        extend: 'selected',
                        text: 'Edit',
                        name: 'edit'
                    },
                    {
                        extend: 'selected',
                        text: 'Delete',
                        name: 'delete',
                    },
                    {
                        text: 'Refresh',
                        name: 'refresh'
                    },
                ],
                onAddRow: function(datatable, rowdata, success, error) {
                    $.ajax({
                        url: "{{ route('client/bond/create') }}",
                        type: 'POST',
                        data: rowdata,
                        success: success,
                        error: error,
                    });
                },
                onEditRow: function(datatable, rowdata, success, error) {
                    $.ajax({
                        url: "{{ route('client/bond/update') }}",
                        type: 'POST',
                        data: rowdata,
                        success: success,
                        error: error,
                    });
                },
                onDeleteRow: function(datatable, rowdata, success, error) {
                    $.ajax({
                        url: "{{ route('client/bond/delete') }}",
                        type: 'POST',
                        data: {
                            bondId: rowdata[0].bondId,
                        },
                        success: success,
                        error: error,
                    });
                },
            });
        }
    </script>
@endsection
