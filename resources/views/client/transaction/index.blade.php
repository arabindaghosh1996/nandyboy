@extends('layouts.main')

@section('content')
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-graph1 icon-gradient bg-asteroid">
                    </i>
                </div>
                <div>Transactions
                    <div class="page-title-subheading">
                        View all transactions
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tabs-animation">
        <div class="mb-3 card">
            <div class="card-body">
                <table id="main_table" class="table table-bordered table-hover dt-responsive nowrap"></table>
            </div>
        </div>
    </div>


    <script>
        window.lib = {
            table: null,
            load_count: 0,
            columnDefs: {},
            user_options: {},
            wallet_options: {},
            transaction_type_options: {
                transfer: 'transfer',
                add: 'add',
                subtract: 'subtract',
                buy: 'buy',
                sell: 'sell',
            },
        };

        $(document).ready(function() {

            // Load data from the server
            $.ajax({
                url: "{{ route('client/user/get_all_name') }}",
                type: 'GET',
                dataType: 'json',
                success: function(response) {
                    response.forEach(function(item) {
                        lib.user_options[item.userId] = item.name;
                    });

                    lib.load_count++;
                    load_done();
                },
                processData: false,
                contentType: false,
            });


            $.ajax({
                url: "{{ route('client/wallet/get_all_name') }}",
                type: 'GET',
                dataType: 'json',
                success: function(response) {
                    response.forEach(function(item) {
                        lib.wallet_options[item.walletId] = item.wallet_name;
                    });

                    lib.load_count++;
                    load_done();
                },
                processData: false,
                contentType: false,
            });
        });

        function load_done() {
            if (lib.load_count >= 2) {
                load_config();
                load_table();
            }
        }

        function load_config() {
            lib.columnDefs = [{
                    data: 'transactionId',
                    title: 'transactionId',
                    type: 'readonly',
                },
                {
                    data: 'from_userId',
                    title: 'from_userId',
                    type: 'select',
                    options: lib.user_options,
                    select2: {
                        width: '100%'
                    },
                    render: function(data, type, row, meta) {
                        if (data == null || !(data in lib.user_options)) return null;
                        return lib.user_options[data];
                    },
                },
                {
                    data: 'to_userId',
                    title: 'to_userId',
                    type: 'select',
                    options: lib.user_options,
                    select2: {
                        width: '100%'
                    },
                    render: function(data, type, row, meta) {
                        if (data == null || !(data in lib.user_options)) return null;
                        return lib.user_options[data];
                    },
                },
                {
                    data: 'from_walletId',
                    title: 'from_walletId',
                    type: 'select',
                    options: lib.wallet_options,
                    select2: {
                        width: '100%'
                    },
                    render: function(data, type, row, meta) {
                        if (data == null || !(data in lib.wallet_options)) return null;
                        return lib.wallet_options[data];
                    },
                },
                {
                    data: 'to_walletId',
                    title: 'to_walletId',
                    type: 'select',
                    options: lib.wallet_options,
                    select2: {
                        width: '100%'
                    },
                    render: function(data, type, row, meta) {
                        if (data == null || !(data in lib.wallet_options)) return null;
                        return lib.wallet_options[data];
                    },
                },
                {
                    data: 'from_account',
                    title: 'from_account',
                },
                {
                    data: 'to_account',
                    title: 'to_account',
                },
                {
                    data: 'transaction_type',
                    title: 'transaction_type',
                    type: 'select',
                    options: lib.transaction_type_options,
                    select2: {
                        width: '100%'
                    },
                    render: function(data, type, row, meta) {
                        if (data == null || !(data in lib.transaction_type_options)) return null;
                        return lib.transaction_type_options[data];
                    },
                },
                {
                    data: 'transaction_amount',
                    title: 'transaction_amount',
                    render: $.fn.dataTable.render.number(',', '.', 0, '₹'),
                },
                {
                    data: 'transaction_time',
                    title: 'transaction_time',
                    datetimepicker: {
                        timepicker: true,
                        format: 'Y-m-d H:i:s'
                    },
                    render: function(data, type, row) {
                        if (type === "sort" || type === "type") {
                            return data;
                        }
                        return moment(data).format("Do MMM YYYY, hh:mm:ss a");
                    },
                },
                {
                    data: 'description',
                    title: 'description',
                },
            ];
        }

        function load_table() {
            lib.table = $('#main_table').DataTable({
                'sPaginationType': 'full_numbers',
                ajax: {
                    url: "{{ route('client/transaction/get') }}",
                    dataSrc: ''
                },
                columns: lib.columnDefs,
                dom: 'Bfrtip',
                select: 'single',
                responsive: true,
                altEditor: true,
                buttons: [
                    // {
                    //     text: 'Add',
                    //     name: 'add'
                    // },
                    {
                        extend: 'selected',
                        text: 'Edit',
                        name: 'edit'
                    },
                    {
                        text: 'Refresh',
                        name: 'refresh'
                    },
                ],
                onEditRow: function(datatable, rowdata, success, error) {
                    $.ajax({
                        url: "{{ route('client/transaction/update') }}",
                        type: 'POST',
                        data: rowdata,
                        success: success,
                        error: error,
                    });
                },
            });
        }
    </script>
@endsection
