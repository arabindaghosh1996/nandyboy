@extends('layouts.main')

@section('content')
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-users icon-gradient bg-asteroid">
                    </i>
                </div>
                <div>Users
                    <div class="page-title-subheading">
                        View all users
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tabs-animation">
        <div class="mb-3 card">
            <div class="card-body">
                <table id="main_table" class="table table-bordered table-hover dt-responsive nowrap"></table>
            </div>
        </div>
    </div>


    <script>
        window.lib = {
            table: null,
            columnDefs: {},
            user_options: {},
            transaction_type_options: {
                transfer: 'transfer',
                add: 'add',
                subtract: 'subtract',
                buy: 'buy',
                sell: 'sell',
            },
        };

        $(document).ready(function() {

            // Load data from the server
            $.ajax({
                url: "{{ route('client/user/get_all_name') }}",
                type: 'GET',
                dataType: 'json',
                success: function(response) {
                    response.forEach(function(item) {
                        lib.user_options[item.userId] = item.name;
                    });

                    load_config();
                    load_table();
                },
                processData: false,
                contentType: false,
            });
        });

        function load_config() {
            lib.columnDefs = [{
                    data: 'userId',
                    title: 'userId',
                    type: 'readonly',
                },
                {
                    data: 'lastLogin',
                    title: 'lastLogin',
                },
                {
                    data: 'email',
                    title: 'email',
                },
                {
                    data: 'name',
                    title: 'name',
                },
                {
                    data: 'phone_num',
                    title: 'phone_num',
                },
                // {
                //     data: 'password',
                //     title: 'password',
                // },
                {
                    data: 'occupation',
                    title: 'occupation',
                },
                {
                    data: 'adhar_number',
                    title: 'adhar_number',
                },
                {
                    data: 'annual_Income',
                    title: 'annual_Income',
                },
                {
                    data: 'agent',
                    title: 'agent',
                },
                {
                    data: 'dob',
                    title: 'dob',
                    datetimepicker: {
                        timepicker: true,
                        format: 'Y-m-d'
                    },
                },
                {
                    data: 'address',
                    title: 'address',
                },
                {
                    data: 'wallet1',
                    title: 'wallet1',
                },
                {
                    data: 'wallet2',
                    title: 'wallet2',
                },
                {
                    data: 'wallet3',
                    title: 'wallet3',
                },
                {
                    data: 'wallet4',
                    title: 'wallet4',
                },
                {
                    data: 'wallet5',
                    title: 'wallet5',
                },
                {
                    data: 'referal_code',
                    title: 'referal_code',
                },
                {
                    data: 'refered_by',
                    title: 'refered_by',
                },
                {
                    data: 'admin',
                    title: 'admin',
                },
                {
                    data: 'cheque',
                    title: 'cheque',
                },
                // {
                //     data: 'token',
                //     title: 'token',
                // },
                {
                    data: 'aadharf',
                    title: 'aadharf',
                },
                {
                    data: 'aadharb',
                    title: 'aadharb',
                },
                {
                    data: 'licensef',
                    title: 'licensef',
                },
                {
                    data: 'licenseb',
                    title: 'licenseb',
                },
                {
                    data: 'pan',
                    title: 'pan',
                },
            ];
        }

        function load_table() {
            lib.table = $('#main_table').DataTable({
                sPaginationType: 'full_numbers',
                ajax: {
                    url: "{{ route('client/user/get') }}",
                    dataSrc: ''
                },
                columns: lib.columnDefs,
                dom: 'Bfrtip',
                select: 'single',
                responsive: true,
                altEditor: true,
                buttons: [{
                    text: 'Refresh',
                    name: 'refresh'
                }, ],
            });
        }
    </script>
@endsection
