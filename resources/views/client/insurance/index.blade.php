@extends('layouts.main')

@section('content')
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-attention icon-gradient bg-asteroid">
                    </i>
                </div>
                <div>Insurances
                    <div class="page-title-subheading">
                        View all insurances
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tabs-animation">
        <div class="mb-3 card">
            <div class="card-body">
                <table id="main_table" class="table table-bordered table-hover dt-responsive nowrap"></table>
            </div>
        </div>
    </div>


    <script>
        window.lib = {
            table: null,
            load_count: 0,
            columnDefs: {},
            user_options: {},
            insurance_status_options: {
                '0': 'Pending',
                '1': 'Accept by agent',
                '2': 'Final accept',
                '3': 'Reject',
            },
            i_update_options: {
                '0': 'Pending',
                '1': 'Updated by agent after admin request',
            },
            wallet_options: {
                'wallet1': 'wallet1',
                'wallet5': 'wallet5',
            },
            currency_options: {
                'inr': 'inr',
                'bsr': 'bsr',
            },
        };

        $(document).ready(function() {

            // Load data from the server
            $.ajax({
                url: "{{ route('client/user/get_all_name') }}",
                type: 'GET',
                dataType: 'json',
                success: function(response) {
                    response.forEach(function(item) {
                        lib.user_options[item.userId] = item.name;
                    });

                    lib.load_count++;
                    load_done();
                },
                processData: false,
                contentType: false,
            });
        });

        function load_done() {
            if (lib.load_count >= 1) {
                load_config();
                load_table();
            }
        }

        function load_config() {
            lib.columnDefs = [{
                    data: 'insuranceId',
                    title: 'insuranceId',
                    type: 'readonly',
                },
                {
                    data: 'userId',
                    title: 'userId',
                    type: 'select',
                    options: lib.user_options,
                    select2: {
                        width: '100%'
                    },
                    render: function(data, type, row, meta) {
                        if (data == null || !(data in lib.user_options)) return null;
                        return lib.user_options[data];
                    },
                },
                {
                    data: 'insurance_status',
                    title: 'insurance_status',
                    type: 'select',
                    options: lib.insurance_status_options,
                    select2: {
                        width: '100%'
                    },
                    render: function(data, type, row, meta) {
                        if (data == null || !(data in lib.insurance_status_options)) return null;
                        return lib.insurance_status_options[data];
                    },
                },
                {
                    data: 'insurance_amount',
                    title: 'insurance_amount',
                },
                {
                    data: 'insurance_type',
                    title: 'insurance_type',
                },
                {
                    data: 'agent_name',
                    title: 'agent_name',
                },
                {
                    data: 'duration',
                    title: 'duration',
                },
                {
                    data: 'i_update',
                    title: 'i_update',
                    type: 'select',
                    options: lib.i_update_options,
                    select2: {
                        width: '100%'
                    },
                    render: function(data, type, row, meta) {
                        if (data == null || !(data in lib.i_update_options)) return null;
                        return lib.i_update_options[data];
                    },
                },
                {
                    data: 'image',
                    title: 'image',
                },
                {
                    data: 'wallet',
                    title: 'wallet',
                    type: 'select',
                    options: lib.wallet_options,
                    select2: {
                        width: '100%'
                    },
                    render: function(data, type, row, meta) {
                        if (data == null || !(data in lib.wallet_options)) return null;
                        return lib.wallet_options[data];
                    },
                },
                {
                    data: 'currency',
                    title: 'currency',
                    type: 'select',
                    options: lib.currency_options,
                    select2: {
                        width: '100%'
                    },
                    render: function(data, type, row, meta) {
                        if (data == null || !(data in lib.currency_options)) return null;
                        return lib.currency_options[data];
                    },
                },
            ];
        }

        function load_table() {
            lib.table = $('#main_table').DataTable({
                'sPaginationType': 'full_numbers',
                ajax: {
                    url: "{{ route('client/insurance/get') }}",
                    dataSrc: ''
                },
                columns: lib.columnDefs,
                dom: 'Bfrtip',
                select: 'single',
                responsive: true,
                altEditor: true,
                buttons: [{
                        text: 'Add',
                        name: 'add',
                    },
                    {
                        extend: 'selected',
                        text: 'Edit',
                        name: 'edit'
                    },
                    {
                        extend: 'selected',
                        text: 'Delete',
                        name: 'delete',
                    },
                    {
                        text: 'Refresh',
                        name: 'refresh'
                    },
                ],
                onAddRow: function(datatable, rowdata, success, error) {
                    $.ajax({
                        url: "{{ route('client/insurance/create') }}",
                        type: 'POST',
                        data: rowdata,
                        success: success,
                        error: error,
                    });
                },
                onEditRow: function(datatable, rowdata, success, error) {
                    $.ajax({
                        url: "{{ route('client/insurance/update') }}",
                        type: 'POST',
                        data: rowdata,
                        success: success,
                        error: error,
                    });
                },
                onDeleteRow: function(datatable, rowdata, success, error) {
                    $.ajax({
                        url: "{{ route('client/insurance/delete') }}",
                        type: 'POST',
                        data: {
                            insuranceId: rowdata[0].insuranceId,
                        },
                        success: success,
                        error: error,
                    });
                },
            });
        }
    </script>
@endsection
