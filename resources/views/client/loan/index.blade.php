@extends('layouts.main')

@section('content')
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-piggy icon-gradient bg-asteroid">
                    </i>
                </div>
                <div>Loans
                    <div class="page-title-subheading">
                        View all loans
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tabs-animation">
        <div class="mb-3 card">
            <div class="card-body">
                <table id="main_table" class="table table-bordered table-hover dt-responsive nowrap"></table>
            </div>
        </div>
    </div>


    <script>
        window.lib = {
            table: null,
            load_count: 0,
            columnDefs: {},
            user_options: {},
            loan_status_options: {
                '0': 'Pending',
                '1': 'Accept by agent',
                '2': 'Final accept',
                '3': 'Reject',
            },
            l_update_options: {
                '0': 'Pending',
                '1': 'Updated by agent after admin request',
            },
            reqinr_options: {
                '0': 'Not requested',
                '1': 'Requested',
                '2': 'Accepted',
                '3': 'Rejected',
            },
        };

        $(document).ready(function() {

            // Load data from the server
            $.ajax({
                url: "{{ route('client/user/get_all_name') }}",
                type: 'GET',
                dataType: 'json',
                success: function(response) {
                    response.forEach(function(item) {
                        lib.user_options[item.userId] = item.name;
                    });

                    lib.load_count++;
                    load_done();
                },
                processData: false,
                contentType: false,
            });
        });

        function load_done() {
            if (lib.load_count >= 1) {
                load_config();
                load_table();
            }
        }

        function load_config() {
            lib.columnDefs = [{
                    data: 'loanId',
                    title: 'loanId',
                    type: 'readonly',
                },
                {
                    data: 'userId',
                    title: 'userId',
                    type: 'select',
                    options: lib.user_options,
                    select2: {
                        width: '100%'
                    },
                    render: function(data, type, row, meta) {
                        if (data == null || !(data in lib.user_options)) return null;
                        return lib.user_options[data];
                    },
                },
                {
                    data: 'loan_status',
                    title: 'loan_status',
                    type: 'select',
                    options: lib.loan_status_options,
                    select2: {
                        width: '100%'
                    },
                    render: function(data, type, row, meta) {
                        if (data == null || !(data in lib.loan_status_options)) return null;
                        return lib.loan_status_options[data];
                    },
                },
                {
                    data: 'loan_amount',
                    title: 'loan_amount',
                },
                {
                    data: 'loan_type',
                    title: 'loan_type',
                },
                {
                    data: 'agent_name',
                    title: 'agent_name',
                },
                {
                    data: 'rate_of_Interest',
                    title: 'rate_of_Interest',
                },
                {
                    data: 'duration',
                    title: 'duration',
                },
                {
                    data: 'l_update',
                    title: 'l_update',
                    type: 'select',
                    options: lib.l_update_options,
                    select2: {
                        width: '100%'
                    },
                    render: function(data, type, row, meta) {
                        if (data == null || !(data in lib.l_update_options)) return null;
                        return lib.l_update_options[data];
                    },
                },
                {
                    data: 'image',
                    title: 'image',
                },
                {
                    data: 'reqinr',
                    title: 'reqinr',
                    type: 'select',
                    options: lib.reqinr_options,
                    select2: {
                        width: '100%'
                    },
                    render: function(data, type, row, meta) {
                        if (data == null || !(data in lib.reqinr_options)) return null;
                        return lib.reqinr_options[data];
                    },
                },
            ];
        }

        function load_table() {
            lib.table = $('#main_table').DataTable({
                'sPaginationType': 'full_numbers',
                ajax: {
                    url: "{{ route('client/loan/get') }}",
                    dataSrc: ''
                },
                columns: lib.columnDefs,
                dom: 'Bfrtip',
                select: 'single',
                responsive: true,
                altEditor: true,
                buttons: [{
                        text: 'Add',
                        name: 'add',
                    },
                    {
                        extend: 'selected',
                        text: 'Edit',
                        name: 'edit'
                    },
                    {
                        extend: 'selected',
                        text: 'Delete',
                        name: 'delete',
                    },
                    {
                        text: 'Refresh',
                        name: 'refresh'
                    },
                ],
                onAddRow: function(datatable, rowdata, success, error) {
                    $.ajax({
                        url: "{{ route('client/loan/create') }}",
                        type: 'POST',
                        data: rowdata,
                        success: success,
                        error: error,
                    });
                },
                onEditRow: function(datatable, rowdata, success, error) {
                    $.ajax({
                        url: "{{ route('client/loan/update') }}",
                        type: 'POST',
                        data: rowdata,
                        success: success,
                        error: error,
                    });
                },
                onDeleteRow: function(datatable, rowdata, success, error) {
                    $.ajax({
                        url: "{{ route('client/loan/delete') }}",
                        type: 'POST',
                        data: {
                            loanId: rowdata[0].loanId,
                        },
                        success: success,
                        error: error,
                    });
                },
            });
        }
    </script>
@endsection
