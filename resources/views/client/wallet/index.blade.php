@extends('layouts.main')

@section('content')
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-credit icon-gradient bg-asteroid">
                    </i>
                </div>
                <div>Wallets
                    <div class="page-title-subheading">
                        View all wallets
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tabs-animation">
        <div class="mb-3 card">
            <div class="card-body">
                <table id="main_table" class="table table-bordered table-hover dt-responsive nowrap"></table>
            </div>
        </div>
    </div>


    <script>
        window.lib = {
            table: null,
            load_count: 0,
            columnDefs: {},
            user_options: {},
            wallet_options: {},
            transaction_type_options: {
                transfer: 'transfer',
                add: 'add',
                subtract: 'subtract',
                buy: 'buy',
                sell: 'sell',
            },
        };

        $(document).ready(function() {

            // Load data from the server
            $.ajax({
                url: "{{ route('client/user/get_all_name') }}",
                type: 'GET',
                dataType: 'json',
                success: function(response) {
                    response.forEach(function(item) {
                        lib.user_options[item.userId] = item.name;
                    });

                    lib.load_count++;
                    load_done();
                },
                processData: false,
                contentType: false,
            });


            // Load data from the server
            $.ajax({
                url: "{{ route('client/main/get_all_coin_name') }}",
                type: 'GET',
                dataType: 'json',
                success: function(response) {
                    response.forEach(function(item) {
                        lib.wallet_options[item.coinId] = item.coin_name;
                    });

                    lib.load_count++;
                    load_done();
                },
                processData: false,
                contentType: false,
            });
        });

        function load_done() {
            if (lib.load_count >= 2) {
                load_config();
                load_table();
            }
        }

        function load_config() {
            lib.columnDefs = [{
                    data: 'walletId',
                    title: 'walletId',
                    type: 'readonly',
                },
                {
                    data: 'userId',
                    title: 'userId',
                    type: 'readonly',
                    // type: 'select',
                    options: lib.user_options,
                    select2: {
                        width: '100%'
                    },
                    render: function(data, type, row, meta) {
                        if (data == null || !(data in lib.user_options)) return null;
                        return lib.user_options[data];
                    },
                },
                {
                    data: 'coinId',
                    title: 'coinId',
                    type: 'readonly',
                    // type: 'select',
                    options: lib.wallet_options,
                    select2: {
                        width: '100%'
                    },
                    render: function(data, type, row, meta) {
                        if (data == null || !(data in lib.wallet_options)) return null;
                        return lib.wallet_options[data];
                    },
                },
                {
                    data: 'wallet_name',
                    title: 'wallet_name',
                    type: 'readonly',
                },
                {
                    data: 'coin',
                    title: 'coin',
                },
            ];
        }

        function load_table() {
            lib.table = $('#main_table').DataTable({
                'sPaginationType': 'full_numbers',
                ajax: {
                    url: "{{ route('client/wallet/get') }}",
                    dataSrc: ''
                },
                columns: lib.columnDefs,
                dom: 'Bfrtip',
                select: 'single',
                responsive: true,
                altEditor: true,
                buttons: [{
                        extend: 'selected',
                        text: 'Edit',
                        name: 'edit'
                    },
                    {
                        text: 'Refresh',
                        name: 'refresh'
                    },
                ],
                onEditRow: function(datatable, rowdata, success, error) {
                    $.ajax({
                        url: "{{ route('client/wallet/update') }}",
                        type: 'POST',
                        data: rowdata,
                        success: success,
                        error: error,
                    });
                },
            });
        }
    </script>
@endsection
